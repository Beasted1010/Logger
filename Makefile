
CC = gcc
INCDIR = inc
SRCDIR = src
OBJDIR = obj
CFLAGS = -Iinc/ -I../inc -Wall

OBJECTS = main.o logger.o
INCLUDES = logger.h

OUT = logger

$(OUT) : $(OBJDIR)/$(OBJECTS) $(INCDIR)/$(INCLUDES)
	$(CC) $(CFLAGS) $(OBJDIR)/%.o -o $(OUT)


$(OBJDIR)/%.o : $(SRCDIR)/%.c
	cd obj && $(CC) -c $(CFLAGS) ../$(SRCDIR)/%.c



#obj/main.o : src/main.c inc/logger.h
#	cd obj/
#	$(CC) -c $(CFLAGS) src/main.c
#	cd ..

#obj/logger.o : src/logger.c
#	cd obj/
#	$(CC) -c $(CFLAGS) src/logger.c
#	cd ..

